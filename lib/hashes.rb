# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split
  lengths = Hash.new(0)

  words.each do |w|
    lengths[w] = w.length
  end

  lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = word.chars
  freqs = Hash.new(0)

  letters.each do |ch|
    freqs[ch] += 1
  end

  freqs
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniqs = Hash.new(0)

  arr.each do |n|
    uniqs[n] += 1
  end

  uniqs.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  freqs = Hash.new(0)

  numbers.each do |n|
    n.even? ? freqs[:even] += 1 : freqs[:odd] += 1
  end

  freqs
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  freqs = Hash.new(0)
  vowels = 'aeiouAEIOU'

  string.chars { |ch| freqs[ch] += 1 if vowels.include?(ch) }

  freqs.sort_by { |k, v| v}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  f_w = Hash.new(0)

  students.each do |k, v|
    f_w[k] = v if v > 6
  end

  combinations(f_w)
end

def combinations(students)
  students.keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  freqs = Hash.new(0)
  num_species = specimens.uniq.count

  specimens.each { |s| freqs[s] += 1 }
  pop_sizes = freqs.sort_by { |k, v| v }
  smallest_pop_size = pop_sizes.first.last
  largest_pop_size = pop_sizes.last.last

  (num_species ** 2) * smallest_pop_size / largest_pop_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_chars = character_count(normal_sign)
  vandal_chars = character_count(vandalized_sign)

  vandal_chars.each_key do |k|
    if normal_chars[k].nil? || normal_chars[k] < vandal_chars[k]
      return false
    end
  end

  true
end

def character_count(str)
  freqs = Hash.new(0)
  puncs = '!?,.;:'
  clean_str = str.downcase.delete(puncs).delete(' ')

  clean_str.chars { |ch| freqs[ch] += 1 }

  freqs
end
